var webpack = require("webpack");

module.exports = {
    entry: './index',
    output: {
        filename: 'browser-bundle.js'
    },
    devtool: 'source-map',  //eval - source-map
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"' // production - development
        }),
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            compress: {
                warnings: false
            }
        })
    ],
    module: {
        loaders: [{
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
            plugins: ['transform-decorators-legacy', 'transform-es2015-destructuring'],
            presets: ['es2015', 'react', 'stage-0']
        }
      }]
    }
};
