import { combineReducers } from 'redux';
import promoFormReducer from './promoFormReducer';
import loginFormReducer from './loginFormReducer';
import regFormReducer from './regFormReducer';
import restoreFormReducer from './restoreFormReducer';
import mainInfoReducer from './mainInfoReducer';

export default combineReducers({
    mainInfo: mainInfoReducer,
    promoForm: promoFormReducer,
    loginForm: loginFormReducer,
    regForm: regFormReducer,
    restoreForm: restoreFormReducer
});