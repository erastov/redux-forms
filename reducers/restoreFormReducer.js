import * as types from '../action-types';

const initialRestoreForm = {
    contact: '',
    login: '',
    error: '',
    isMultiContact: false,
    finalText: ''
};

const restoreFormReducer = (state = initialRestoreForm, action) => {
    switch (action.type) {
        case types.CHANGE_CONTACT_RESTORE_FORM:
            return {
                ...state,
                isMultiContact: false,
                contact: action.contact
            };
        case types.SUBMIT_RESTORE_FORM:
            return {
                ...state,
                error: action.error,
                isMultiContact: action.isMultiContact,
                finalText: action.finalText
            };
        case types.CHANGE_LOGIN_RESTORE_FORM:
            return {
                ...state,
                login: action.login
            };
        case types.SUCCESS_RESTORE:
        case types.BACK_FROM_RESTORE_FORM:
            return initialRestoreForm;
        default:
            return state;
    }
};


export default restoreFormReducer;