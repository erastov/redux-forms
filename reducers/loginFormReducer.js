import * as types from '../action-types';

const initialLoginForm = {
    step: 1,
    login: '',
    password: '',
    loginError: '',
    passwordError: '',
    isHasContact: false
};

const loginFormReducer = (state = initialLoginForm, action) => {
    switch (action.type) {
        case types.CHANGE_LOGIN:
            return {
                ...state,
                login: action.login
            };
        case types.SUBMIT_LOGIN:
            return {
                ...state,
                step: action.step,
                loginError: action.loginError
            };
        case types.CHANGE_PASSWORD:
            return {
                ...state,
                password: action.password
            };
        case types.SUCCESS_RESTORE:
            return {
                ...state,
                password: '',
                passwordError: ''
            };
        case types.SUBMIT_PASSWORD:
            return {
                ...state,
                isHasContact: action.isHasContact,
                passwordError: action.passwordError
            };
        case types.GO_TO_FIRST_STEP:
            return {
                ...state,
                step: 1,
                passwordError: '',
                password: ''
            };
        case types.CLEAR_FORM:
            return initialLoginForm;
        default:
            return state;
    }
};


export default loginFormReducer;