import * as types from '../action-types';

const initialPromoForm = {
    promo: '',
    promoError: '',
    isCompleted: false
};

const promoFormReducer = (state = initialPromoForm, action) => {
    switch (action.type) {
        case types.CHANGE_PROMO:
            return {
                ...state,
                promo: action.promo
            };
        case types.SET_FINAL_TEXT:
            return {
                ...state,
                finalText: action.finalText
            };
        case types.SUBMIT_PROMO:
            return {
                ...state,
                isCompleted: action.isCompleted,
                promoError: action.promoError
            };
        default:
            return state;
    }
};

export default promoFormReducer;