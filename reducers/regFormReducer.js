import * as types from '../action-types';

const initialRegForm = {
    isActive: false,
    phone: '',
    phoneError: ''
};

const regFormReducer = (state = initialRegForm, action) => {
    switch (action.type) {
        case types.TOGGLE_REG_FORM:
            return {
                ...state,
                isActive: !state.isActive
            };
        case types.CHANGE_PHONE:
            return {
                ...state,
                phone: action.phone
            };
        case types.SUBMIT_PHONE:
            return {
                ...state,
                phoneError: action.phoneError
            };
        default:
            return state;
    }
};

export default regFormReducer;