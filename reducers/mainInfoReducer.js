import * as types from '../action-types';

const initialMainState = {
    activeForm: types.PROMO_FORM,
    finalText: ''
};

const mainInfoReducer = (state = initialMainState, action) => {
    switch (action.type) {
        case types.CHANGE_ACTIVE_FORM:
            return {
                ...state,
                activeForm: action.activeForm
            };
        case types.SET_FINAL_TEXT:
            return {
                ...state,
                finalText: action.finalText
            };
        case types.SUCCESS_RESTORE:
            return {
                ...state,
                activeForm: types.LOGIN_FORM
            };
        default:
            return state;
    }
};

export default mainInfoReducer;