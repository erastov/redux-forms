import React from 'react';
import ErrorLine from '../helpers/ErrorLine';

export const LoginForm = (props) => {
    let text = '',
        bottomText;

    bottomText = (
        <div className="row message">
            <p className="link"><a onClick={e => {e.preventDefault(); props.onToggleRegForm()}} href="#">или зарегистрируйтесь</a></p>
        </div>
    );

    switch(props.step) {
        case (1) :
            if (props.loginError.length > 0) {
                if (props.loginError[0] == 'Введите логин.') {
                    bottomText = <ErrorLine text={props.loginError} />;
                } else {
                    bottomText = (
                        <div className="row message">
                            <p className="text"><span className="triangle-span"></span> Мы пока незнакомы. <a onClick={e => {e.preventDefault(); props.onToggleRegForm()}} href="#">Зарегистрируйтесь:)</a></p>
                        </div>
                    );
                }
            }

            text = (
                <form name="loginForm" className="loginForm" onSubmit={e => {e.preventDefault(); props.submitLogin()}}>
                    <div className="row breadcrumbs">
                        <p>
                            <span className="step_login"><i>1</i> <b>Укажите логин</b></span>
                        </p>
                    </div>
                    <div className="row fields">
                        <div className="inputs">
                            <p className="clearfix">
                                <input autoComplete="off" autoFocus onChange={e => props.changeLogin(e.target.value.trim())} value={props.login} type="text" name="LoginForm[login]" className="form_data" maxLength="30" />
                                <input type="hidden" name="LoginForm[step]" value="1" />
                                <input type="submit" name="step_2_submit" className="form_submit"  value=' ' />
                            </p>
                        </div>
                    </div>
                </form>
            );
            break;
        case (2) :
            let helpText = '';
            if (props.passwordError.length) {
                if (props.isHasContact) {
                    helpText = (
                        <a href="" onClick={e => {e.preventDefault(); props.onGoToRestore();}}>Напомнить?</a>
                    );
                } else {
                    helpText = "Увы, мы не можем найти ваши контактные данные. Позвоните по телефону 8-800-555-3234, и мы поможем вам!";
                }
                helpText = (
                    <div className="row message no-padding">
                        <p className="text"><span className="triangle-span" />{props.passwordError} {helpText}</p>
                    </div>
                );
            }

            text = (
                <form name="loginForm" className="loginForm" onSubmit={e => {e.preventDefault(); props.submitPassword()}}>
                    <div className="row breadcrumbs">
                        <p>
                            <a onClick={e => {e.preventDefault(); props.firstStepLogin()}} href="#">
                                <span className="step_login"><i>1</i> <b>Укажите логин</b></span>
                            </a>
                            <span className="step_passw"><i>2</i> <b>Укажите пароль</b></span>
                        </p>
                    </div>
                    <div className="row fields">
                        <div className="inputs">
                            <p className="clearfix">
                                <input
                                    autoComplete="off" autoFocus
                                   onChange={e => props.changePassword(e.target.value.trim())}
                                   value={props.password}
                                   type="password"
                                   name="LoginForm[password]"
                                   className="form_data"
                                />
                                <input type="hidden" name="LoginForm[login]" value={props.login} />
                                <input type="hidden" name="LoginForm[step]" value="2" />
                                <input type="hidden" name="LoginForm[promo]" value={props.promo} />
                                <input type="submit" name="step_2_submit" className="form_submit"  value=' ' />
                            </p>
                        </div>
                    </div>
                    {helpText}
                </form>
            );
            break;
    }

    return (
        <div>
            {text}
            {bottomText}
        </div>
    )
};

LoginForm.propTypes = {
    step: React.PropTypes.number.isRequired,
    login: React.PropTypes.string.isRequired,
    isHasContact: React.PropTypes.bool.isRequired,
    password: React.PropTypes.string.isRequired,
    passwordError: React.PropTypes.array,
    loginError: React.PropTypes.array,
    changeLogin: React.PropTypes.func.isRequired,
    submitLogin: React.PropTypes.func.isRequired,
    changePassword: React.PropTypes.func.isRequired,
    submitPassword: React.PropTypes.func.isRequired,
    firstStepLogin: React.PropTypes.func.isRequired,
    promo: React.PropTypes.string.isRequired,
    onToggleRegForm: React.PropTypes.func.isRequired,
    onGoToRestore: React.PropTypes.func.isRequired
};

export default LoginForm;
