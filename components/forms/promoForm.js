import React from 'react';
import ErrorLine from '../helpers/ErrorLine';

export const PromoForm = (props) => {
    let innerText;
    if (props.isCompleted == 1) {
        innerText = (
            <div className="row label">
                <p>
                    <label htmlFor="PromoForm[promo]">Ваш промокод:</label>
                    <span>{props.promo}</span>
                </p>
            </div>
        );
    } else {
        innerText = (
            <form id="promoForm" name="promoForm" action="" method="POST" className="PromoForm" onSubmit={(e) => {e.preventDefault(); props.submitPromoForm(props.promo)}}>
                <div className="row label">
                    <p><label htmlFor="PromoForm[promo]">Введите ваш промокод:</label></p>
                </div>
                <div className="row fields">
                    <div className="inputs">
                        <p className="clearfix">
                            <input autoComplete="off" onChange={e => props.changePromo(e.target.value.trim())} type="text" value={props.promo} name="PromoForm[promo]" className="form_data" />
                            <input type="submit" name="step_1_submit" className="form_submit" value=' ' />
                        </p>
                    </div>
                </div>
                <ErrorLine text={props.promoError} />

                <div class="row agreemet">
                    <p>Если вы не знаете промокод, обратитесь к менеджеру e2e4 по телефону 8-800-555-3234</p>
                </div>
            </form>
        );
    }

    return innerText;
};

PromoForm.propTypes = {
    isCompleted: React.PropTypes.bool.isRequired,
    promo: React.PropTypes.string.isRequired,
    promoError: React.PropTypes.array,
    promoForm: React.PropTypes.object.isRequired,
    changePromo: React.PropTypes.func.isRequired,
    submitPromoForm: React.PropTypes.func.isRequired
};

export default PromoForm;