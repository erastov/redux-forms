import React from 'react';
import ErrorLine from '../helpers/ErrorLine';

export const RestoreForm = props => {
    let input = '';

    if (props.isMultiContact) {
        input = (
            <p className="clearfix">
                <input autoFocus autoComplete="off" onChange={e => props.changeLogin(e.target.value.trim())} type="text" value={props.login} name="RestoreForm[login]" className="form_data" />
                <input type="submit" name="step_1_submit" className="form_submit" value="" />
            </p>
        );
    }
    if (props.finalText) {
        return (
            <div className="row centered">
                <div className="row message">
                    <p className="link"><span />{props.finalText} <br/> <a onClick={e => {e.preventDefault(); props.goToLoginForm()}} href="">Перейти к вводу пароля.</a></p>
                </div>
            </div>
        );
    } else {
        return (
            <form id="restoreForm" name="restoreForm" action="" method="POST" className="restoreForm" onSubmit={(e) => {e.preventDefault(); props.submitRestoreForm()}}>
                <div className="row label">
                    <p><label htmlFor="RestoreForm[contact]">Введите ваш email или сотовый телефон:</label></p>
                </div>
                <div className="row fields">
                    <div className="inputs">
                        <p className="clearfix">
                            <input autoFocus={input ? false : true} autoComplete="off" onChange={e => props.changeContact(e.target.value.trim())} type="text" value={props.contact} name="RestoreForm[contact]" className="form_data" />
                            {input ? '' : <input type="submit" name="step_1_submit" className="form_submit"  value=' ' />}
                        </p>
                        {input}
                    </div>
                </div>
                <ErrorLine text={props.error} errorClass="no-padding" />
                <div className="row message">
                    <p className="link"><a onClick={e => {e.preventDefault(); props.goToLoginForm()}} href="#">назад</a></p>
                </div>
            </form>
        )
    }
};

RestoreForm.propTypes = {
    changeContact: React.PropTypes.func.isRequired,
    changeLogin: React.PropTypes.func.isRequired,
    submitRestoreForm: React.PropTypes.func.isRequired,


    isMultiContact: React.PropTypes.bool.isRequired,
    login: React.PropTypes.string.isRequired,
    finalText: React.PropTypes.string.isRequired,
    contact: React.PropTypes.string.isRequired,
    error: React.PropTypes.string
};

export default RestoreForm;