import React from 'react';
import ErrorLine from '../helpers/ErrorLine';
import ReactMaskMixin from 'react-mask-mixin';

const InputMask = React.createClass({
  mixins: [ReactMaskMixin],
  render() {
    return <input {...this.props} {...this.mask.props}/>
  }
});

export const RegForm = (props) => {
    return (
        <form name="RegForm" className="RegForm" onSubmit={e => {e.preventDefault(); props.submitPhone()}}>
            <div className="row label">
                <p><label htmlFor="RegForm[phone]">Создадим вам профиль на номер:</label></p>
            </div>
            <div className="row fields">
                <div className="inputs">
                    <p className="clearfix">
                        <InputMask
                            autoComplete="off"
                            onChange={e => props.changePhone(e.target.value)}
                            autoFocus
                            name="RegForm[phone]"
                            value={props.phone}
                            className="form_data"
                            mask="8-999-999-99-99"
                            size="20"
                            placeholder="8-999-999-99-99"
                        />
                        <input type="hidden" name="RegForm[promo]" value={props.promo} />
                        <input type="submit" name="step_2_submit" className="form_submit"  value=' ' />
                    </p>
                </div>
            </div>
            <ErrorLine errorClass="no-padding" text={props.phoneError} />
            <div className="row message">
                <p className="link"><a onClick={e => {e.preventDefault(); props.onToggleRegForm()}} href="#">назад</a></p>
            </div>
        </form>
    );
};

RegForm.propTypes = {
    phone: React.PropTypes.string.isRequired,
    phoneError: React.PropTypes.string,
    changePhone: React.PropTypes.func.isRequired,
    submitPhone: React.PropTypes.func.isRequired,
    onToggleRegForm: React.PropTypes.func.isRequired,
    promo: React.PropTypes.string.isRequired
};

export default RegForm;