import React, { Component } from 'react';
import PromoForm from './forms/promoForm';
import LoginForm from './forms/loginForm';
import RegForm from './forms/regForm';
import RestoreForm from './forms/restoreForm';
import SuccessText from './helpers/successText';
import { bindActionCreators } from 'redux';
import * as promoActionCreators from '../actions/promoActions';
import * as loginActionCreators from '../actions/loginActions';
import * as regActionCreators from '../actions/regActions';
import * as restoreActionCreators from '../actions/restoreActions';
import * as mainInfoActionCreators from '../actions/mainInfoActions';
import { connect } from 'react-redux'
import { PROMO_FORM, LOGIN_FORM, RESTORE_FORM, REG_FORM } from '../action-types';

@connect(
    state => ({
        mainInfo: state.mainInfo,
        restoreForm: state.restoreForm,
        promoForm: state.promoForm,
        loginForm: state.loginForm,
        regForm: state.regForm
    }),
    dispatch => ({
        promoActions: bindActionCreators(promoActionCreators, dispatch),
        loginActions: bindActionCreators(loginActionCreators, dispatch),
        regActions: bindActionCreators(regActionCreators, dispatch),
        restoreActions: bindActionCreators(restoreActionCreators, dispatch),
        mainInfoActions: bindActionCreators(mainInfoActionCreators, dispatch)
    })
)

export default class App extends Component {

    goToRegForm = () => {
        this.props.loginActions.clearForm();
        this.props.mainInfoActions.changeActiveForm(REG_FORM);
    };

    goToLoginForm = () => {
        this.props.mainInfoActions.changeActiveForm(LOGIN_FORM);
    };

    goToRestoreForm = () => {
        this.props.mainInfoActions.changeActiveForm(RESTORE_FORM);
    };

    renderPromoForm() {
        return <PromoForm
            {...this.props.promoForm}
            {...this.props.promoActions}
        />;
    }

    renderLoginForm() {
        return <LoginForm
            {...this.props.loginForm}
            {...this.props.loginActions}
            onToggleRegForm={this.goToRegForm}
            promo={this.props.promoForm.promo}
            onGoToRestore={this.goToRestoreForm}
        />;
    }

    renderRegForm() {
        return <RegForm
            {...this.props.regForm}
            onToggleRegForm={this.goToLoginForm}
            {...this.props.regActions}
            promo={this.props.promoForm.promo}
        />;
    }

    renderRestoreForm() {
        return <RestoreForm
            {...this.props.restoreActions}
            restoreForm={this.props.restoreForm}
        />;
    }

    getActiveForm(activeForm) {
        let form = '';

        switch (activeForm) {
            case PROMO_FORM:
                form = '';
                break;
            case LOGIN_FORM:
                form = this.renderLoginForm();
                break;
            case REG_FORM:
                form = this.renderRegForm();
                break;
            case RESTORE_FORM:
                form = this.renderRestoreForm();
                break;
        }

        return form;
    }

    render() {
        const { mainInfo } = this.props;

        if (mainInfo.finalText.length) {
            return <SuccessText text={mainInfo.finalText} />;
        }

        let form = this.getActiveForm(mainInfo.activeForm);

        return (
            <div className="row centered">
                {this.renderPromoForm()}
                {form}
                <div className="row agreement">
                    <p><sup>*</sup> подключаясь к проекту, вы даете согласие на обработку ваших персональных данных.</p>
                </div>
            </div>
        );
    }
};