import React from 'react';

export const SuccessText = (props) => (
    <div id="step_4" className="row centered">
        <div className="row label">
            <p><label>{props.text}</label></p>
        </div>
    </div>
);

SuccessText.propTypes = {
    text: React.PropTypes.string.isRequired
};

export default SuccessText;

