import React from 'react';

export const ErrorLine = (props) => {
    if (props.text) {
        let errorClass = props.errorClass ? props.errorClass : '';
        return (
            <div className={"row message " + errorClass}>
                <p className="text"><span className="triangle-span" />{props.text}</p>
            </div>
        );
    } else {
        return <div></div>;
    }
};

ErrorLine.propTypes = {
    text: React.PropTypes.array,
    errorClass: React.PropTypes.string
};

export default ErrorLine;