import * as types from '../action-types';
import { setFinalText } from './mainInfoActions';

export const changeLogin = login => ({
    type: types.CHANGE_LOGIN,
    login
});

export const processSubmitLogin = response => ({
    type: types.SUBMIT_LOGIN,
    loginError: response.errors.hasOwnProperty('login') ? response.errors.login : '',
    step: response.errors.hasOwnProperty('login') ? 1: 2
});

export function submitLogin() {
    return dispatch => {
        return fetch('/api/process-login-form', {
            method: 'post',
            body: new FormData(document.querySelector('.loginForm')),
            credentials: 'same-origin'
        })
        .then(response => response.json())
        .then(response => dispatch(processSubmitLogin(response)))
    }
}

export const changePassword = password => ({
    type: types.CHANGE_PASSWORD,
    password
});

export const processSubmitPassword = response => ({
    type: types.SUBMIT_PASSWORD,
    passwordError: response.errors.hasOwnProperty('password') ? response.errors.password : '',
    isHasContact: response.isHasContact,
    isCompleted: !response.errors.hasOwnProperty('password')
});

export const submitPassword = () => {
    return dispatch => {
        return fetch('/api/process-login-form', {
            method: 'post',
            body: new FormData(document.querySelector('.loginForm')),
            credentials: 'same-origin'
        })
            .then(response => response.json())
            .then(response => {
                dispatch(processSubmitPassword(response));
                return response;
            })
            .then(response => dispatch(setFinalText(response.finalText)))
    }
};

export const firstStepLogin = () => ({
    type: types.GO_TO_FIRST_STEP
});

export const clearForm = () => ({
    type: types.CLEAR_FORM
});