import * as types from '../action-types';

export const changeContact = (contact) => ({
    type: types.CHANGE_CONTACT_RESTORE_FORM,
    contact
});

export const changeLogin = (contact) => ({
    type: types.CHANGE_LOGIN_RESTORE_FORM,
    contact
});

export const processRestoreResponse = response => ({
    type: types.SUBMIT_RESTORE_FORM,
    error: response.error,
    isMultiContact: response.isMultiContact,
    finalText: response.finalText
});

export const goToLoginForm = (finalText) => ({
    type: types.SUCCESS_RESTORE
});

export function submitRestoreForm() {
    return dispatch => {
        return fetch('/api/restore', {
            method: 'post',
            body: new FormData(document.querySelector('.restoreForm')),
            credentials: 'same-origin'
        })
        .then(response => response.json())
        .then(response => {
            dispatch(processRestoreResponse(response));
        })
    }
}