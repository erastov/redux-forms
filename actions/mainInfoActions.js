import * as types from '../action-types';

export const changeActiveForm = form => ({
    type: types.CHANGE_ACTIVE_FORM,
    activeForm: form
});

export const setFinalText = finalText => ({
    type: types.SET_FINAL_TEXT,
    finalText
});