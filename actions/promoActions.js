import * as types from '../action-types';
import { changeActiveForm } from './mainInfoActions';

export const changePromo = promo => ({
    type: types.CHANGE_PROMO,
    promo
});

export const processResponse = response => ({
    type: types.SUBMIT_PROMO,
    promoError: response.errors.hasOwnProperty('promo') ? response.errors.promo : '',
    isCompleted: !response.errors.hasOwnProperty('promo')
});

export function submitPromoForm() {
    return dispatch => {
        return fetch('/api/validate-promo', {
            method: 'post',
            body: new FormData(document.querySelector('.PromoForm')),
            credentials: 'same-origin'
        })
        .then(response => response.json())
        .then(response => {
            dispatch(processResponse(response));
            return response;
        })
        .then(response => {
            if (response.errors.hasOwnProperty('promo') == false) {
                dispatch(changeActiveForm(types.LOGIN_FORM));
            }
            return response;
        })
    }
}