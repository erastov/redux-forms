import * as types from '../action-types';
import { setFinalText } from './mainInfoActions';

export const changePhone = phone => ({
    type: types.CHANGE_PHONE,
    phone
});

const processResponse = response => {
    let isHasError = response.errors.hasOwnProperty('phone');
    return {
        type: types.SUBMIT_PHONE,
        phoneError: isHasError ? response.errors.phone : '',
        isCompleted: !isHasError
    }
};

export function submitPhone() {
    return dispatch => {
        return fetch('/api/process-reg-form', {
            method: 'post',
            body: new FormData(document.querySelector('.RegForm')),
            credentials: 'same-origin'
        })
        .then(response => response.json())
        .then(response => {
            dispatch(processResponse(response));
            return response;
        })
        .then(response => dispatch(setFinalText(response.finalText)))
    }
}